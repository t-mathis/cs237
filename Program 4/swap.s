*----------------------------------------------------------------------
* Programmer: Troy Mathis
* Class Account: masc1125
* Filename: swap.s
*----------------------------------------------------------------------
*
* Register use
* D1 - Used temporarily to store a swapped value.
* A0 - Used for address of first value to be swapped
* A1 - Used for address of second value to be swapped.
*
*----------------------------------------------------------------------
	ORG 	$8000
swap:	
	link		A6,#0
	movem.l		A0/A1/D1,-(SP)
	movea.l		8(A6),A0
	movea.l		12(A6),A1
	move.b		(A0),D1
	move.b		(A1),(A0)
	move.b		D1,(A1)
	movem.l		(SP)+,A0/A1/D1
	unlk		A6
	rts
	end
