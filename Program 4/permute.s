*----------------------------------------------------------------------
* Programmer: Troy Mathis
* Class Account: masc1125
* Filename: permute.s
*----------------------------------------------------------------------
*
#minclude /home/ma/cs237/bsvc/iomacs.s
#minclude /home/ma/cs237/bsvc/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use
* A0 - Used for address of the array.
* A1 - Used for address of first char in sub-list.
* A2 - Used for address of last char in sub-list.
* A3 - Used for the loop and swap sub calls.
* A4 - Used temporarily to increment first for the permute sub call.
*
*----------------------------------------------------------------------
		ORG 	$7000
thearray:	EQU	$6002		* The array starts after the first word.
swap: 		EQU	$8000
permute:
	initIO
	link 		A6,#0
	movem.l 	A0-A4,-(SP)	
	movea.l 	8(A6),A0	*Address of first char in list.
	movea.l		12(A6),A1	*Address of first char in sub-list.
	movea.l		16(A6),A2	*Address of last char in sub-list.
	  
	cmpa.l		A2,A1		*if(first == last)
	beq 		printarr
	
	movea.l		A1,A3		*Setting up the loop. (int i = first)
begloop:
	cmpa.l		A2,A3		*Exit loop if i > last.
	bhi		done
	  
	pea		(A3)		*Parameter: b.
	pea		(A1)		*Parameter: a.
	jsr		swap		*First call to sub swap.
	adda.l		#8,SP		
	
	move.l		A1,A4		*Make a copy of first. (first+1)
	addq.l		#1,A4
	pea		(A2)		*Parameter: Last.
	pea		(A4)		*Parameter: First+1.
	pea		(A0)		*Parameter: First element of the array.
	jsr		permute		*Recursive call to permute.
	adda.l		#12,SP
	  
	pea		(A3)		*Parameter: b.
	pea		(A1)		*Parameter: a.
	jsr		swap		*Second call to sub swap.
	adda.l		#8,SP
	  
	addq.l		#1,A3		*Increment i. (i++)
	bra		begloop
 
printarr:
	tst.b		(A2)		*Fix for strange bug.
	bne		done
	lineout 	thearray	*Print. Already NULL terminated.
done:	movem.l		(SP)+,A0-A4
	unlk		A6
 	rts
	end
