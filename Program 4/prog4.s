*----------------------------------------------------------------------
* Programmer: Troy Mathis
* Class Account: masc1125
* Assignment or Title: Permutations
* Filename: prog4.s
* Date completed:  04/28/2014
*----------------------------------------------------------------------
* Problem statement: Write a program that generates all possible 
*		     permutations of a list of characters
* Input: N/A
* Output: Permutations of the given list of characters in data.s.
* Error conditions tested: N\A
* Included files: prog4.s, permute.s, swap.s. A data.s is used as well.
* Method and/or pseudocode: Call recursive subroutine permute, recurse,
*			    print lists of permutations.
* References: 
* Version 1.0 - 04/28/2014 - Objective completed.
* Version 1.1 - 05/07/2014 - Fixed bug, documentation, goal.
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/ma/cs237/bsvc/iomacs.s
#minclude /home/ma/cs237/bsvc/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use
* D0 - Temporarily used for arithmetic.
* A0 - Used for address of the array.
* A1 - Used for address of first char in array.
* A2 - Used for address of last char in array.
*
*----------------------------------------------------------------------
*
permute:	EQU	$7000
data:		EQU	$6000
start:  initIO                  	* Initialize (required for I/O)
	setEVT				* Error handling routines
*	initF				* For floating point macros only	

					* Your code goes HERE
	lineout 	title
	lea		data+2,A2
	move.w		data,D0
	adda.l		D0,A2		* Parameter: Last char in array.
	lea		data+2,A1	* Parameter: First char in array.
	lea		data+2,A0	* Parameter: The array.
	
	pea		(A2)
	pea		(A1)
	pea		(A0)
	jsr		permute		* The initial call to sub permute.
	adda.l		#12,SP

        break                   	* Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

				* Your storage declarations go 
				* HERE
title		dc.b	'Program #4, masc1125, Troy Mathis',0
        end
