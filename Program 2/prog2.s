*----------------------------------------------------------------------
* Programmer: Troy Mathis
* Class Account: masc1125
* Assignment or Title: "What Day is it?" - Program #2
* Filename: prog2.s
* Date completed:  03/07/2014
*----------------------------------------------------------------------
* Problem statement: Write a program that takes an input a date, and 
* 		     prints the day of the week for that date.
* Input: Prompts the user for a date in (MM/DD/YYYY) format.
* Output: Displays the day of the inputted date.
* Error conditions tested: none
* Included files: prog2.s
* Method and/or pseudocode: Prompt the user for a date in (MM/DD/YYYY)
* 			    format.Calculate a, m, y, and then d using
*                           provided formulas. Use d to find the day of
*                           the week, then lineout the result.
* References: 
* Version 1.0 - 03/07/14 - Objective completed.
* Version 1.1 - 03/20/14 - Optimized the code slightly. Added documentation.
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/ma/cs237/bsvc/iomacs.s
#minclude /home/ma/cs237/bsvc/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use:
* D0 - Interacting with macros. 'd' gets placed into here.
* D1 - The 'd' variable in the formula.
* D2 - The 'm' variable in the formula.
* D3 - The 'y' variable in the formula.
* D4 - The 'a' variable in the formula.
* D5 - Temporarily used for calculations.
* D6 - Temporarily used for calculations.
*
* A2 - Used for actualdate and moving the day of the week into it.
* A3 - Used for the date strings to move the right day into actualdate.
*
*----------------------------------------------------------------------
*
start:  initIO                  	* Initialize (required for I/O)
	setEVT				* Error handling routines
*	initF				* For floating point macros only	
					
	lineout		title		* Your code goes HERE
	lineout		askfordate
	linein		buffer		* (Code reused from my prog1.)
	cvta2		buffer+0,#2	* MM is 0 from left, 2 chars.
	move.w		D0,inputMM
	cvta2		buffer+3,#2	* DD is 3 from left, 2 chars.
	move.w		D0,inputDD
	cvta2		buffer+6,#4	* YYYY is 6 from left, 4 chars.
	move.w		D0,inputYY
		
	clr.l		D1		* Clear data registers of any nonsense.
	clr.l		D0
	
	** a = (14 - inputMM) / 12
	clr.l		D4		* Find (a) and put it in D4.
	move.w		inputMM,D1
	move.w		#14,D0
	sub.l		D1,D0
	move.w		#12,D1
	divs		D1,D0
	move.w		D0,D4		* (a) is in D4 now.
	
	** y = inputYY - a
	clr.l		D3		* Find (y) and put it in D3.
	move.w		inputYY,D3
	sub.l		D4,D3		* (y) is in D3 now.
	
	** m = inputMM + (12 * a) - 2
	clr.l		D2		* Find (m) and put it in D2.
	move.w		inputMM,D2
	move.l		D4,D5
	muls		#12,D5
	add.l		D5,D2
	clr.l		D5
	move.b		#2,D5
	sub.l		D5,D2		* (m) is in D2 now.
	
	** d = (inputDD + y + (y / 4) - (y / 100) + (y / 400) + ((31 * m) / 12)) % 7
	clr.l		D1		* Find (d) and put it in D1.
	move.w		inputDD,D1	* Start the calculations.
	add.l		D3,D1		* + y
	
	clr.l		D5		* + (y / 4)
	move.l		#4,D5
	move.l		D3,D6
	divs		D5,D6
	move.w		D6,D5
	add.l		D5,D1
	
	clr.l		D5		* - (y / 100)
	move.l		#100,D5
	move.l		D3,D6
	divs		D5,D6
	move.w		D6,D5
	sub.l		D5,D1
	
	clr.l		D5		* + (y / 400)
	move.l		#400,D5
	move.l		D3,D6
	divs		D5,D6
	move.w		D6,D5
	add.l		D5,D1
	
	clr.l		D5		* + ((31 * m) / 12)
	clr.l		D6
	move.l		D2,D5
	muls		#31,D5
	move.l		#12,D6
	divs		D6,D5
	move.w		D5,D6
	add.l		D6,D1
	
	clr.l		D5		* (above results) % 7
	move.l		D1,D5
	divs		#7,D5
	swap		D5		* Swap for remainder.	
	move.w		D5,D1		* (d) is in D1 now.

	muls		#12,D1		* Get the start of the day string.	
	lea		actualdate,A2
	lea		daystrings,A3
	adda.l		D1,A3		
	move.l		(A3)+,(A2)+
	move.l		(A3)+,(A2)+
	move.l		(A3)+,(A2)+
	
	clr.b		(A2)		* NULL terminate result.
	lineout		result

        break                   	* Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

					* Your storage declarations go 
					* HERE
title:		dc.b	'Program #2, Troy Mathis, masc1125',0
askfordate:	dc.b	'Enter a date (MM/DD/YYYY):',0
buffer:		ds.b	80
inputDD:	ds.w	1
inputMM:	ds.w	1
inputYY		ds.w	1
daystrings:	dc.b	'Sunday.     '	* Longest day is Wednesday, so
		dc.b	'Monday.     '	* make all the strings the same.
		dc.b	'Tuesday.    ' 	* Period added to strings because
		dc.b	'Wednesday.  '	* days vary in length. Make them 
		dc.b	'Thursday.   '  * 12 characters each for 3 longs.
		dc.b	'Friday.     '
		dc.b	'Saturday.   '
result:		dc.b	'That day is a '
actualdate:	ds.b	36		* 36 bytes again.
        end
