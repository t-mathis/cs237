*----------------------------------------------------------------------
* Programmer: Troy Mathis	
* Class Account: masc1125
* Assignment or Title: Programming Assignment #3
* Filename: prog3.s
* Date completed:  03/26/2014
*----------------------------------------------------------------------
* Problem statement: Write a program that prints the prime factors for 
*		     any unsigned integer of size word. 
* Input: Prompt the user to enter an integer between 2 and 65535.
* Output: The factors of the inputted integer, followed by asking the
*         user if he wants to do it again.
* Error conditions tested: Validates the input is a number and is in
*                          the range of 2..65535.
* Included files: prog3.s
* Method and/or pseudocode: Prompt the user for an integer, validated it,
*                           and branch based on validation. Calculate
*			    the factors, then ask the user to go again.
* References: 
* Version 1.0 - 03/26/14 - Completed the objective.
* Version 1.1 - 04/21/14 - Added register use and my name. Fixed bug.
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/ma/cs237/bsvc/iomacs.s
#minclude /home/ma/cs237/bsvc/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use
* D0 - Used for working with macros.
* D1 - Keeps the inputted number, and the current number to be factored.
* D2 - Keeps the current factor being used.
* D3 - Temporarily keeps the inputted number/current number for arithmetic.
* D4 - Temporarily keeps the current factor being used for arithmetic.
* D5 - Temporarily used for arithmetic.
* D6 - Used to build 'The factors of...' string.
* D7 - Used for input validation.
*
* A0 - Used for input validation, then for building output strings.
* A1 - Used for building output strings.
*----------------------------------------------------------------------
*
start:  initIO                  * Initialize (required for I/O)
	setEVT			* Error handling routines
*	initF			* For floating point macros only	

				* Your code goes HERE
	lineout		title
	
enterint:
	lineout		entryprompt	* Ask the user for an integer
	linein		buffer
	move.l		D0,D6		* Store the string length for later.
	move.l		D0,D7		* (D6) for the string, (D7) for validation.
	
	lea		buffer,A0       * Validate each input digit is a number.
	subq		#1,D7
	
validatenum:
	cmpi.b		#'0',(A0)
	blt		badint
	cmpi.b		#'9',(A0)
	bgt		badint
	adda.l		#1,A0
	dbra		D7,validatenum
	
	cvta2		buffer,D0	* Convert to 2's compliment.	
	cmp.l		#2,D0		* Validate input integer. 2 < i < 65535
	blt		badint
	cmp.l		#65535,D0
	bgt		badint
	bra		goodint

badint:
	lineout		entryerror	* Tell the user their entry was invalid.
	bra		enterint	* Make them enter it again.
	
goodint:	
	move.l		D0,D1		* (D1) Number (n)
	move.l		#2,D2		* (D2) Factor (f) = 2
	
	lea		buffer,A0	* Build the string for the 'The factors
	lea		factorsmsg,A1	* of XXXX are:' part.
	adda.l		#15,A1
	
buildstring:
	move.b		(A0)+,(A1)+	* Adds the number to the string.
	dbra		D6,buildstring
	suba.l		#1,A1
	move.b		#' ',(A1)+	* Adds ' are:' to the string.
	move.b		#'a',(A1)+
	move.b		#'r',(A1)+
	move.b		#'e',(A1)+
	move.b		#':',(A1)+
	clr.b		(A1)		* NULL terminate that string.
	
	lea		finalans,A0	* (A0) Final answer containing factors.
	clr.l		D0
	clr.l		D6
	
factorloop:				* Begin the FOR loop.
	cmp.l		D1,D2		* Do loop: factor <= number
	bgt		endloop		* End loop: factor > number
	
	move.l		D1,D3		* Use (D3) and (D4) for temp work.
	move.l		D2,D4
	divs		D4,D3		* Performs (n / f)
	clr.l		D5
	move.w		D3,D5		* (D5) Quotient for (n / f)
	swap		D3
	ext.l		D3		* (D3) Remainder for (n / f)
	
	cmp.l		#0,D3		
	beq		doif		* if(number % factor == 0)
	bra		endif
doif:
	move.l		D2,D0		* Performs answer += factor + "*"
	cvt2a		currans,#6	* to the answer, finalans.
	stripp		currans,#6
	move.l		D0,D6
	lea		currans,A1
answer:	
	move.b		(A1)+,(A0)+
	dbra		D6,answer
	suba.l		#1,A0
	move.b		#'*',(A0)+
	
	move.l		D5,D1		* Moves quotient of (n / f) to number.
	subq.l		#1,D2		* factor--
endif:	
	addq.l		#1,D2		* factor++
	bra		factorloop	* Completed FOR loop.
	
endloop:
	clr.b		-1(A0)		* NULL terminate finalans and remove the excess '*'.
	lineout		factorsmsg
	lineout		finalans

doanother:
	lineout		againprompt	* Ask the user if they want to do it again.
	linein		buffer
	
	cmpi.b		#1,D0		* Make sure the user entered a single digit.
	bne		doanother
	
	ori.b		#$20,buffer	* YyNn are valid responses.
	beq		enterint
	cmpi.b		#'y',buffer
	beq		enterint
	cmpi.b		#'n',buffer
	beq		DONE
	lineout		ynerror		* Response was not YyNn.
	bra		doanother
DONE
        break                   	* Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

					* Your storage declarations go 
					* HERE
title:		dc.b	'Program #3, Troy Mathis, masc1125',0
buffer:		ds.b	80
entryprompt:	dc.b	'Enter an integer to factor (2..65535):',0
againprompt:	dc.b	'Do you want to factor another integer (Y/N)?',0
entryerror:	dc.b	'Sorry, your input is not a valid integer.',0
ynerror:	dc.b	'Sorry, your input is not a valid response.',0
currans:	ds.b	10
finalans:	ds.b	120
factorsmsg:	dc.b	'The factors of '
        end
