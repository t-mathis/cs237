*----------------------------------------------------------------------
* Programmer: Alan Riggins  
* Assignment or Title: Demo for trap14, floating point macros
* Filename: trap14demo.s
* Date completed:  August 23, 2012
*----------------------------------------------------------------------
* Problem statement:
*   This file demonstrates usage of the trap 14 floating point macros
*   developed for the bsvc system.
*
* Input: floating point numbers X and Y entered by the user
*
* Output: The two numbers X and Y with the various floating point 
*   routines.
*
* Error conditions tested: None.
*
* Included files: iomacs.s, evtmacs.s
*
* Method and/or pseudocode: 
*	output greeting
*	do {
*		prompt user for input
*		read input lines for X and Y
*		echo user's lines
*		perform various arithmetic operations on X and Y
*		query user about continuing
*	   }
*	while user enters Y|y to continue
*
* References:  none.
*
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
#minclude /home/ma/cs237/bsvc/iomacs.s
#minclude /home/ma/cs237/bsvc/evtmacs.s


start:  initIO                  * Initialize (required for I/O)
        initF                   * Initialize floating point macros
        setEVT                  * Enable error handling
        
        lineout stars           * Output heading
        lineout title
        lineout stars
continue:
        lineout skipln
        lineout prompt1
        floatin buffer
        cvtaf   buffer,D1       * X
        lineout prompt2
        floatin buffer
        cvtaf   buffer,D2       * Y
        ** Now echo the numbers X and Y to screen
        move.l  D1,D0
        cvtfa   input1,#6
        move.l  D2,D0
        cvtfa   input2,#6
        

        ** addition
        move.l  D2,D0       * copy of Y
        fadd.l  D1,D0
        cvtfa   plus,#6     * the macro reads input from D0 only!
                            * write answer at plus, with six digits right of decimal pt.
        
        ** subtraction
        move.l  D1,D0       * D0 gets X
        fsub    D2,D0       * D0 get X - Y
        cvtfa   sub,#6
        
        ** multiplication
        move.l  D2,D0       * copy of Y
        fmul    D1,D0
        cvtfa   times,#6
        
        ** division
        move.l  D1,D0       * copy of X
        fdiv    D2,D0       * D0 gets X / Y
        cvtfa   div,#6
        
        ** square root
        move.l  D1,D3
        fsqrt   D3,D0       * take sqrt of D3 and put in D0
        cvtfa   sqrt1,#6
        
        move.l  D2,D3
        fsqrt   D3,D0
        cvtfa   sqrt2,#6
        
        ** power function 
        ** This function can be VERY SLOW!
        move.l  D2,D0
        fpow    D1,D0       X ^ Y -> D0
        cvtfa   power,#6

        lineout echo1
        lineout echo2
        ** now check the compare function
        fcmp    D2,D1       * if X (condition) Y
        bgt     greater
        blt     less
        lineout cmpE
        bra     nextprint
greater:
        lineout cmpG
        bra     nextprint
less:   lineout cmpL
nextprint:                
        lineout ans1
        lineout ans2
        lineout ans3
        lineout ans4
        lineout ans5
        lineout ans6
        lineout ans7                                                
        
        ** Does user want to go again?
        lineout skipln
        lineout query           * Query user about continuing
        linein  buffer
        move.b  buffer,D1
        cmp.b   #'Y',D1
        beq     continue 
        cmp.b   #'y',D1
        beq     continue
        lineout bye
        break                   * Terminate execution
*
*       Storage declarations

title:  dc.b    '***   Welcome to the floating point demo   ***',0
stars:  dc.b    '**********************************************',0
prompt1: dc.b    'Please enter the first number X',0
prompt2: dc.b    'Please enter the second number Y',0        
query:  dc.b    'Do you want to enter another set of numbers (y/n)?',0
bye:    dc.b    'The program is finished.',0
skipln: dc.b    0
buffer: ds.b    82
echo1:  dc.b    'X is: '
input1  ds.b    30
echo2:  dc.b    'Y is: '
input2  ds.b    30
cmpL    dc.b    'X < Y',0
cmpE    dc.b    'X == Y',0
cmpG    dc.b    'X > Y',0
ans1:   dc.b    'X + Y is: '
plus    ds.b    30
ans2:   dc.b    'X - Y is: '
sub     ds.b    30
ans3:   dc.b    'X * Y is: '
times:  ds.b    30
ans4    dc.b    'X / Y is: '
div:    ds.b    30
ans5:   dc.b    'The square root of X is: '
sqrt1:   ds.b    30
ans6:   dc.b    'The square root of Y is: '
sqrt2:  ds.b    30
ans7:   dc.b    'X raised to power Y ( X^Y ) is: '
power:  ds.b    30
        end
