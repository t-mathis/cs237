*----------------------------------------------------------------------
* Programmer: Troy Mathis
* Class Account: masc1125
* Assignment or Title: Programming Assignment #1
* Filename: prog1.s
* Date completed:  3/1/2014
*----------------------------------------------------------------------
* Problem statement: Write a program to calculate the user's age in 
*                    2014 given the user's date of birth.
* Input: Prompt the user for his DOB in (MM/DD/YYYY) format.
* Output: Display the age the user will be in 2014.
* Error conditions tested: none
* Included files: prog1.s
* Method and/or pseudocode: Prompt for the DOB, parse the year, subtract
*                	    years for the age, add ' years old.',
*			    NULL terminate, and lineout result.
* References: 
* Version 1.0 - 03/01/14 - Completed the objective.
* Version 1.1 - 03/06/14 - Allocated more memory to agein2014 for best practices.
* Version 1.2 - 03/07/14 - Added register use documentation. (no code change)
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/ma/cs237/bsvc/iomacs.s
#minclude /home/ma/cs237/bsvc/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use:
* D0 - Interacting with macros.
* D1 - Temporarily used for calculations.
* D2 - Unused.
* D3 - Unused.
* D4 - Unused.
* D5 - Unused.
* D6 - Unused.
* D7 - Unused.
*
*----------------------------------------------------------------------
*
start:  initIO                  	* Initialize (required for I/O)
	setEVT				* Error handling routines
*	initF				* For floating point macros only	

					* Your code goes HERE
	lineout		title
	lineout		askfordob
	linein		buffer
	cvta2		buffer+0,#2	* MM is 0 from left, 2 chars
	move.w		D0,inputMM
	cvta2		buffer+3,#2	* DD is 3 from left, 2 chars
	move.w		D0,inputDD
	cvta2		buffer+6,#4	* YYYY is 6 from left, 4 chars
	move.w		D0,inputYY
	
	move.l		$00000000,D1	* Clear D1 of any nonsense.
	
	move.w		inputYY,D1	* (D1) Inputted year.
	move.w		#2014,D0	* (D0) Current year.
	sub.l		D1,D0		* (D0) Resulting age.
	
	cvt2a		agein2014,#3	* Format the age into agein2014.
	stripp		agein2014,#3
	
	lea		agein2014,A1	* (A1) Age address.
	adda.l		D0,A1
	
	move.b		#' ',(A1)	* Add ' years old.'.
	adda.l		#1,A1
	move.b		#'y',(A1)
	adda.l		#1,A1
	move.b		#'e',(A1)
	adda.l		#1,A1
	move.b		#'a',(A1)
	adda.l		#1,A1
	move.b		#'r',(A1)
	adda.l		#1,A1	
	move.b		#'s',(A1)
	adda.l		#1,A1
	move.b		#' ',(A1)
	adda.l		#1,A1
	move.b		#'o',(A1)
	adda.l		#1,A1
	move.b		#'l',(A1)
	adda.l		#1,A1
	move.b		#'d',(A1)
	adda.l		#1,A1
	move.b		#'.',(A1)
	adda.l		#1,A1

	clr.b		(A1)		* NULL terminate result.
	lineout		result

        break                   	* Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

					* Your storage declarations go 
					* HERE
title: 		dc.b	'Program #1, Troy Mathis, masc1125',0
askfordob:	dc.b	'Enter your date of birth (MM/DD/YYYY): ',0
buffer:		ds.b	80
inputDD:	ds.w	1
inputMM:	ds.w	1
inputYY:	ds.w	1
result:		dc.b	'In 2014, you will be '
agein2014:	ds.b	36		* 36 bytes should be sufficient.
        end
